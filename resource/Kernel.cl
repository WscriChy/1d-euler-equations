#pragma OPENCL EXTENSION cl_khr_fp64 : enable

typedef struct {
  double a11;
  double a12;
  double a13;
  double a21;
  double a22;
  double a23;
  double a31;
  double a32;
  double a33;
} Matrix3;

double3
multiplyByVector(Matrix3 matrix,
                 double3 vector) {
  return (double3)(
    matrix.a11 * vector.x + matrix.a12 * vector.y + matrix.a13 * vector.z,
    matrix.a21 * vector.x + matrix.a22 * vector.y + matrix.a23 * vector.z,
    matrix.a31 * vector.x + matrix.a32 * vector.y + matrix.a33 * vector.z);
}

Matrix3
multiplyByScalar(Matrix3 matrix,
                 double  scalar) {
  Matrix3 multipliedMatrix = {
    matrix.a11 * scalar, matrix.a12 * scalar, matrix.a13 * scalar,
    matrix.a21 * scalar, matrix.a22 * scalar, matrix.a23 * scalar,
    matrix.a31 * scalar, matrix.a32 * scalar, matrix.a33 * scalar
  };
  return multipliedMatrix;
}

Matrix3
inverseMatrix(Matrix3 matrix) {
  double det = matrix.a11 *
               (matrix.a22 * matrix.a33 - matrix.a23 * matrix.a32) -
               matrix.a12 *
               (matrix.a21 * matrix.a33 - matrix.a23 * matrix.a31) +
               matrix.a13 *
               (matrix.a21 * matrix.a32 - matrix.a22 * matrix.a31);
  Matrix3 inverse = {
    matrix.a22 * matrix.a33 - matrix.a23 * matrix.a32,
    matrix.a13 * matrix.a32 - matrix.a12 * matrix.a33,
    matrix.a12 * matrix.a23 - matrix.a13 * matrix.a22,
    matrix.a23 * matrix.a31 - matrix.a21 * matrix.a33,
    matrix.a11 * matrix.a33 - matrix.a13 * matrix.a31,
    matrix.a13 * matrix.a21 - matrix.a11 * matrix.a23,
    matrix.a21 * matrix.a32 - matrix.a22 * matrix.a31,
    matrix.a12 * matrix.a31 - matrix.a11 * matrix.a32,
    matrix.a11 * matrix.a22 - matrix.a12 * matrix.a21
  };
  inverse = multiplyByScalar(inverse, 1 / det);
  return inverse;
}

bool
haveTheSameSign(double l,
                double r) {
  if (l < 0.0)
    return r < 0.0;
  else
    return r >= 0.0;
}

#define doubleMin 0.0
bool
closeToZero(double arg) {
  if (fabs(arg) <= doubleMin)
    return true;

  return false;
}

#define gamma 1.4

Matrix3
fluxMatrix(double u, double h) {
  Matrix3 matrix = {
    0,                                    1,
    0,
    (gamma - 3) * u * u / 2,              (3 - gamma) * u,
    gamma - 1,
    u * (((gamma - 1) * u * u  / 2) - h), h - (gamma - 1) * u * u,
    gamma * u
  };
  return matrix;
};

Matrix3
eigenCoefficientMatrix(double u,
                       double c) {
  double  uSquare        = u * u;
  double  cInverse       = 1 / c;
  double  cInverseSquare = 1 / (c * c);
  double  uCQuotient     = u / c;
  double  gammaMinusOne  = gamma - 1;
  Matrix3 matrix         = {
    uCQuotient* (2 + gammaMinusOne * uCQuotient) / 4,
    -cInverse * (1 + gammaMinusOne * uCQuotient) / 2,
    gammaMinusOne * cInverseSquare / 2,
    1 - gammaMinusOne * uSquare * cInverseSquare / 2,
    gammaMinusOne * u * cInverseSquare,
    -gammaMinusOne * cInverseSquare,
    -uCQuotient * (2 - gammaMinusOne * uCQuotient) / 4,
    cInverse * (1 - gammaMinusOne * uCQuotient) / 2,
    gammaMinusOne * cInverseSquare / 2
  };
  return matrix;
}

Matrix3
eigenMatrix(double u,
            double h,
            double c) {
  Matrix3 matrix = {
    1,         1,           1,
    u - c,     u,           u + c,
    h - u * c, 0.5 * u * u, h + u * c
  };
  return matrix;
}

double3
absEigenVector(double u,
               double c) {
  return (double3)(fabs(u - c), fabs(c), fabs(u + c));
}

double3
eigenVector(double u,
            double c) {
  return (double3)((u - c), (c), (u + c));
}

double
computePressure(double density,
                double velocity,
                double energy) {
  return (gamma - 1) * (energy - 0.5 * density * velocity * velocity);
}

double
computeEnergy(double density,
              double velocity,
              double pressure) {
  return pressure / (gamma - 1) + 0.5 * density * velocity * velocity;
}

double
soundSpeed(double u,
           double h) {
  return sqrt((gamma - 1) * (h - 0.5 * u * u));
}

double
totalEnthalpy(
  double3 vector) {
  double h = gamma * vector.z;

  if (vector.x == 0)
    return 0;

  h /= vector.x;
  double quot = vector.y / vector.x;
  h -= 0.5 * (gamma - 1) * quot * quot;
  return h;
}

double
avrVelocity(
  double3 vector,
  double3 vector2) {
  double roSqrtLeft  = sqrt(vector.x);
  double roSqrtRight = sqrt(vector2.x);
  double u           = 0;

  if (roSqrtLeft != 0)
    u = vector.y / roSqrtLeft;

  if (roSqrtRight != 0)
    u += vector2.y / roSqrtRight;

  if (u != 0)
    u /= roSqrtLeft + roSqrtRight;

  return u;
}

double
avrTotalEnthalpy(
  double3 vector,
  double3 vector2) {
  double roSqrtLeft  = sqrt(vector.x);
  double roSqrtRight = sqrt(vector2.x);
  double hLeft       = totalEnthalpy(vector);
  double hRight      = totalEnthalpy(vector2);
  double h           = roSqrtLeft * hLeft + roSqrtRight * hRight;

  if (h != 0)
    h /= roSqrtLeft + roSqrtRight;

  return h;
}

double3
entropyFix(double  u,
           double  h,
           double3 vector,
           double3 vector2) {
  double  u1           = vector.y / vector.x;
  double  c1           = soundSpeed(u1, totalEnthalpy(vector));
  double  u2           = vector2.y / vector2.x;
  double  c2           = soundSpeed(u2, totalEnthalpy(vector2));
  double  c            = soundSpeed(u, h);
  double3 eigenValues  = eigenVector(u, c);
  double3 eigenValues1 = eigenVector(u1, c1);
  double3 eigenValues2 = eigenVector(u2, c2);
  double3 qDelta       = vector2 - vector;
  double3 fluxFix;

  //   if (closeToZero(eigenValues.x) ||
  //      closeToZero(eigenValues.y) ||
  //     closeToZero(eigenValues.z)) {
  if (false)
    return 0.0, 0.0, 0.0;

  else {
    if (isequal(c, 0.0))
      return 0.0, 0.0, 0.0;

    //  if (closeToZero(eigenValues.x)) {
    //    double beta = (eigenValues2.x - eigenValues.x) /
    //                  (eigenValues2.x - eigenValues1.x);
    //    eigenValues.x = (1 - beta) * eigenValues2.x -
    //                    beta * eigenValues1.x;
    //  } else
    //    eigenValues.x = fabs(eigenValues.x);

    //  if (closeToZero(eigenValues.y)) {
    //    double beta = (eigenValues2.y - eigenValues.y) /
    //                  (eigenValues2.y - eigenValues1.y);
    //    eigenValues.y = (1 - beta) * eigenValues2.y -
    //                    beta * eigenValues1.y;
    //  } else
    //    eigenValues.y = fabs(eigenValues.y);

    //  if (closeToZero(eigenValues.z)) {
    //    double beta = (eigenValues2.z - eigenValues.z) /
    //                  (eigenValues2.z - eigenValues1.z);
    //    eigenValues.z = (1 - beta) * eigenValues2.z -
    //                    beta * eigenValues1.z;
    //  } else
    //    eigenValues.z = fabs(eigenValues.z);

    Matrix3 eigenMatrixValue = eigenMatrix(u, h, c);
    Matrix3 inverseEigenMatrixValue = inverseMatrix(eigenMatrixValue);
    // eigenCoefficientMatrix(u,c);
    double3 eigenCoeff       =
      multiplyByVector(inverseEigenMatrixValue, qDelta);
    fluxFix.x = fabs(eigenValues.x) * eigenCoeff.x;
    fluxFix.y = fabs(eigenValues.y) * eigenCoeff.y;
    fluxFix.z = fabs(eigenValues.z) * eigenCoeff.z;
    //   fluxFix.x = max(fabs(eigenValues1.x), fabs(eigenValues2.x)) *
    //               eigenCoeff.x;
    //   fluxFix.y = max(fabs(eigenValues1.y), fabs(eigenValues2.y)) *
    //               eigenCoeff.y;
    //   fluxFix.z = max(fabs(eigenValues1.z), fabs(eigenValues2.z)) *
    //               eigenCoeff.z;

    fluxFix = multiplyByVector(eigenMatrixValue, fluxFix);
  }

  return fluxFix;
}

double3
flux(
  double3 vector,
  double3 vector2) {
  double u = avrVelocity(vector, vector2);
  double h = avrTotalEnthalpy(vector, vector2);

  double3 qSum = vector2 + vector;

  double3 flux = multiplyByVector(fluxMatrix(u, h), qSum);

  return 0.5 * (flux - entropyFix(u, h, vector, vector2));
}

void
init(
  const unsigned int position,
  const int          xSize,
  __global double*   vector) {
  if (position < (xSize / 2)) {
    vector[0] = 1.0;
    vector[1] = 0.0;
    vector[2] = 1.0;
  } else {
    vector[0] = 0.1;
    vector[1] = 0.0;
    vector[2] = 0.1;
  }
}

void
init2(
  const unsigned int position,
  const int          xSize,
  __global double*   vector) {
  double value = sin((2 * M_PI * position - 0.5) / xSize);
  vector[0] = value * value + 0.2;
  vector[1] = vector[0] * value;
  vector[2] = value * value + 0.2;
}

void
init3(
  const unsigned int position,
  const int          xSize,
  __global double*   vector) {
  double deviation = sqrt(((double)xSize) * 1.0);
  double density   = (100.0 / (deviation * sqrt(2.0 * M_PI))) *
                     exp(-pown((position - xSize / 2.0), 2) /
                         (2.0 * deviation * deviation));
  double velocity = sin(2 * (2 * M_PI * position - 0.5) / xSize);
  vector[0] = density + 0.1;
  vector[1] = vector[0] * velocity;
  vector[2] = computeEnergy(vector[0],
                            velocity,
                            density);
}

typedef struct {
  bool   init;
  double dX;
  int    xSize;
  double dT;
  double t;
} ClConfig;

__kernel void
computeFlux(
  __global const ClConfig* config,
  __global double*         solution,
  __global double*         fluxes) {
  unsigned int       id = get_global_id(0);
  const unsigned int x  = id;

  __global double* currentCell = solution + 3 * x;
  __global double* nextCell    = currentCell + 3;

  __global double* fluxCell = fluxes + 3 * x;

  if (config->init) {
    init(x, config->xSize, currentCell);
    return;
  }

  if (x == 0) {
    currentCell[0] = nextCell[0];
    currentCell[1] = -nextCell[1];
    currentCell[2] = nextCell[2];
  }

  if (x == (config->xSize - 2)) {
    nextCell[0] = currentCell[0];
    nextCell[1] = -currentCell[1];
    nextCell[2] = currentCell[2];
  }

  if (x == (config->xSize - 1))
    return;

  double3 wleft =
    (double3)(currentCell[0], currentCell[1], currentCell[2]);
  double3 wright =
    (double3)(nextCell[0], nextCell[1], nextCell[2]);

  double3 fluxValue = flux(wleft, wright);
  double3 result    =
    (config->dT / config->dX) * fluxValue;
  fluxCell[0] = result.x;
  fluxCell[1] = result.y;
  fluxCell[2] = result.z;
}

__kernel void
updateSolution(
  __global const ClConfig* config,
  __global double*         solution,
  __global double*         fluxes) {
  unsigned int       id = get_global_id(0);
  const unsigned int x  = id;

  __global double* currentCell = solution + 3 * x;

  __global double* rightFlux = fluxes + 3 * x;
  __global double* leftFlux  = rightFlux - 3;

  if ((x != 0) && (x != (config->xSize - 1))) {
    currentCell[0] -= rightFlux[0];
    currentCell[1] -= rightFlux[1];
    currentCell[2] -= rightFlux[2];
    currentCell[0] += leftFlux[0];
    currentCell[1] += leftFlux[1];
    currentCell[2] += leftFlux[2];
  }

  //  if (currentCell[0] != 0.0)
  //    currentCell[1] = currentCell[1] / currentCell[0];
  //  else
  //    currentCell[1] = 0.0;
  //
  //  currentCell[2] = computePressure(currentCell[0],
  //                                   currentCell[1],
  //                                   currentCell[2]);
}

__kernel void
computeTimeStep(
  __global double* solution,
  __global double* maxEigenValues) {
  unsigned int       id = get_global_id(0);
  const unsigned int x  = id;

  __global double* currentCell = solution + 3 * x;

  double rho    = currentCell[0];
  double energy = currentCell[2];
  double u      = currentCell[1] / rho;
  double c      =
    sqrt(gamma * (gamma - 1) * (energy / rho - 0.5 * u * u));
  double3 eigenValues = absEigenVector(u, c);
  double  max         = fmax(eigenValues.x, eigenValues.y);
  max               = fmax(max, eigenValues.z);
  maxEigenValues[x] = max;
}
