#ifndef EulerEquations_Kernel_hpp
#define EulerEquations_Kernel_hpp

#include <fstream>
#include <string>

#include "Cl.hpp"
#include "LoggingMacros.hpp"

#define GAMMA 1.4

namespace EulerEquations {
struct ClConfig {
  cl_bool   init;
  cl_double dX;
  cl_int    xSize;
  // Could be used as init boolean (dT=0)
  cl_double dT;
  cl_double t;
};

class Kernel {
public:
  Kernel() {}

  Kernel(
    double const& dX,
    int const&    xSize):
    _dX(dX),
    _xSize(xSize) {}

  Kernel(
    Kernel const& other) {}

  virtual
  ~Kernel() {}

  Kernel const&
  operator=(
    Kernel const& other) {
    return *this;
  }

  void
  sourceFileName(
    std::string const& sourceFileName) {
    _sourceFileName = sourceFileName;
  }

  void
  dX(double const& dX) { _dX = dX; }

  void
  xSize(int const& xSize) { _xSize = xSize; }

  void
  safetyFactor(double const& safetyFactor) {
    _safetyFactor = safetyFactor;
  } /* ----- end of safetyFactor ----- */

  void
  initialize() {
    try {
      // Get available platforms
      std::vector<Cl::Platform> platforms;
      Cl::Platform::get(&platforms);

      // Select the default platform and create
      // a context using this platform and the GPU
      cl_context_properties cps[] = {
        CL_CONTEXT_PLATFORM,
        (cl_context_properties)(platforms[0])(),
        0
      };
      _context = Cl::Context(CL_DEVICE_TYPE_GPU, cps);

      std::vector<Cl::Device> devices = _context.getInfo<CL_CONTEXT_DEVICES>();

      // Create a command queue and use the first device
      _queue = Cl::CommandQueue(_context, devices[0]);

      std::string sourceCode;
      readSource(sourceCode);
      Cl::Program::Sources source(1, std::make_pair(
                                    sourceCode.c_str(), sourceCode.length() +
                                    1));
      // Make program of the source code in the context
      _program = Cl::Program(_context, source);

      // Build program for these specific devices
      try {
        _program.build(devices);
      } catch (Cl::Error const& exc) {
        std::string buildLog = _program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(
          devices[0]);
        PRINT(buildLog)
        return;
      }

      // Make kernel
      _computingKernel       = Cl::Kernel(_program, "computeFlux");
      _updatingKernel        = Cl::Kernel(_program, "updateSolution");
      _computeTimeStepKernel = Cl::Kernel(_program, "computeTimeStep");

      int resolution = _xSize;

      _solutionSize = resolution * 3 * sizeof(cl_double);

      _solutionBuffer = Cl::Buffer(
        _context,
        // CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR,
        CL_MEM_ALLOC_HOST_PTR,
        _solutionSize,
        0);

      _maxEigenValueBuffer = Cl::Buffer(
        _context,
        // CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR,
        CL_MEM_ALLOC_HOST_PTR,
        _solutionSize / 3,
        0);

      _fluxBuffer = Cl::Buffer(
        _context,
        CL_MEM_READ_WRITE,
        _solutionSize,
        0);

      _solution = (double*)_queue.enqueueMapBuffer(_solutionBuffer,
                                                   CL_TRUE,
                                                   CL_MAP_READ,
                                                   0,
                                                   _solutionSize);
      _maxEigenValues = (double*)_queue.enqueueMapBuffer(_maxEigenValueBuffer,
                                                         CL_TRUE,
                                                         CL_MAP_READ,
                                                         0,
                                                         _solutionSize / 3);

      _clConfigBuffer = Cl::Buffer(
        _context,
        CL_MEM_READ_ONLY,
        sizeof(ClConfig));

      _computingKernel.setArg(0, _clConfigBuffer);
      _computingKernel.setArg(1, _solutionBuffer);
      _computingKernel.setArg(2, _fluxBuffer);

      // Run the kernel on specific ND range
      _global = Cl::NDRange(resolution);
      _local  = Cl::NDRange(resolution);

      _clConfig.init  = true;
      _clConfig.dX    = _dX;
      _clConfig.xSize = _xSize;

      _queue.enqueueWriteBuffer(_clConfigBuffer, CL_TRUE, 0,
                                sizeof(ClConfig), &_clConfig);

      _queue.enqueueNDRangeKernel(_computingKernel,
                                  Cl::NullRange,
                                  _global,
                                  _local);
      _clConfig.init = false;
    } catch (Cl::Error const& exc) {
      PRINT("An error occured during OpenCL initialization at '%s': %i",
            exc.what(), exc.err());
      return;
    }
  }

  void
  release() {
    _queue.enqueueUnmapMemObject(
      _solutionBuffer,
      _solution);

    _queue.enqueueUnmapMemObject(
      _maxEigenValueBuffer,
      _maxEigenValues);
  } /* ----- end of release ----- */

  double
  computeTimeStep() {
    _computeTimeStepKernel.setArg(0, _solutionBuffer);
    _computeTimeStepKernel.setArg(1, _maxEigenValueBuffer);
    _queue.enqueueNDRangeKernel(_computeTimeStepKernel,
                                Cl::NullRange,
                                _global,
                                _local);
    _queue.enqueueReadBuffer(_maxEigenValueBuffer,
                             CL_TRUE,
                             0,
                             _solutionSize / 3,
                             _maxEigenValues);
    double timeStep = _maxEigenValues[0];

    for (int i = 1; i < _xSize; ++i)
      if (timeStep < _maxEigenValues[i])
        timeStep = _maxEigenValues[i];

    timeStep = _safetyFactor * (_dX / (2 * timeStep));
    return timeStep;
  } /* ----- end of computeTimeStep ----- */

  void
  run(
    double&       dT,
    double const& t) {
    _clConfig.t  = t;
    dT           = computeTimeStep();
    _clConfig.dT = dT;
    _queue.enqueueWriteBuffer(_clConfigBuffer, CL_TRUE, 0,
                              sizeof(ClConfig), &_clConfig);
    _computingKernel.setArg(0, _clConfigBuffer);
    _computingKernel.setArg(1, _solutionBuffer);
    _computingKernel.setArg(2, _fluxBuffer);
    _updatingKernel.setArg(0, _clConfigBuffer);
    _updatingKernel.setArg(1, _solutionBuffer);
    _updatingKernel.setArg(2, _fluxBuffer);
    _queue.enqueueNDRangeKernel(_computingKernel,
                                Cl::NullRange,
                                _global,
                                _local);
    _queue.enqueueNDRangeKernel(_updatingKernel, Cl::NullRange, _global,
                                _local);
  } /* ----- end of run ----- */

  double const* const
  solution() const {
    _queue.enqueueReadBuffer(_solutionBuffer,
                             CL_TRUE,
                             0,
                             _solutionSize,
                             _solution);
    double* vector = _solution;

    for (int i = 0; i < _xSize; ++i) {
      if (vector[0] != 0.0) {
        vector[2] =
          (GAMMA - 1) * (vector[2] -
                         0.5 * vector[1] * vector[1] / vector[0]);
        vector[1] /= vector[0];
      }

      vector += 3;
    }

    return _solution;
  }

private:
  void
  readSource(std::string& sourceCode) {
    // Read source file
    std::ifstream sourceFile(_sourceFileName);

    if (!sourceFile.is_open())
      PRINT("Cannot open the file: %s", _sourceFileName.c_str());

    sourceCode.append(
      std::istreambuf_iterator<char>(sourceFile),
      (std::istreambuf_iterator<char>()));
  }

private:
  std::string _sourceFileName;

  double   _dX;
  int      _xSize;
  double   _safetyFactor;
  ClConfig _clConfig;
  double*  _solution;
  double*  _maxEigenValues;

  Cl::Context      _context;
  Cl::CommandQueue _queue;
  Cl::Program      _program;
  Cl::Kernel       _computingKernel;
  Cl::Kernel       _updatingKernel;
  Cl::Kernel       _computeTimeStepKernel;
  Cl::Buffer       _solutionBuffer;
  Cl::Buffer       _fluxBuffer;
  Cl::Buffer       _maxEigenValueBuffer;
  ::size_t  _solutionSize;
  Cl::Buffer  _xVelocityBuffer;
  Cl::Buffer  _yVelocityBuffer;
  Cl::Buffer  _zVelocityBuffer;
  Cl::Buffer  _clConfigBuffer;
  Cl::NDRange _global;
  Cl::NDRange _local;
};   /* ----- end of Kernel ----- */
} /* ----- end of EulerEquations ----- */
#endif /* ----- end EulerEquations_Kernel_hpp ----- */
