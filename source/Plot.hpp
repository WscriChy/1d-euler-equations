#ifndef Ui_Plot_hpp
#define Ui_Plot_hpp

#include <QtCore>

#include "qcustomplot.hpp"
#include "LoggingMacros.hpp"

namespace Ui {
class Plot: public QCustomPlot {
  Q_OBJECT

public:
  Plot(QWidget* parent = 0);

  Plot(
    Plot const& other) {}

  virtual
  ~Plot() {}

  Plot const&
  operator=(
    Plot const& other) {
    return *this;
  }

  void
  toggleAutoRescale() {
    _isAutoRescale = !_isAutoRescale;
  }

public slots:
  void
  render(QCPDataMap* map);

private slots:
  void
  selectionChangedSlot();

  void
  mousePressSlot();

  void
  mouseWheelSlot();

private:
  bool _isAutoRescale;
}; /* ----- end of Plot ----- */
} /* ----- end of Ui ----- */
#endif /* ----- end Ui_Plot_hpp ----- */
