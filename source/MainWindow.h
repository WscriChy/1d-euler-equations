#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>

#include "Plot.hpp"
#include "Renderer.hpp"

namespace Ui {
class MainWindow: public QMainWindow {
  Q_OBJECT

public:
  explicit
  MainWindow(QWidget* parent = 0);

  ~MainWindow();

protected:
  void
  showEvent(QShowEvent* event);

  void
  closeEvent(QCloseEvent* event);

private:
  void
  createActions();

  void
  createToolBars();

private slots:
  void
  runSimulation();

  void
  tileMdiWindows();

  void
  toggleAutoReslcalePlots();

  void
  reslcalePlots();

  void
  increaseDelay();

  void
  reduceDelay();

  void
  close();

private:
  QMdiArea*                _mdiArea;
  Plot*                    _densityPlot;
  Plot*                    _velocityPlot;
  Plot*                    _pressurePlot;
  EulerEquations::Renderer _renderer;

  QToolBar* _toolBar;
  QAction*  _runSimulationAction;
  QAction*  _layoutAction;
  QAction*  _rescaleAction;
  QAction*  _autoRescaleAction;
  QAction*  _increaseDelayAction;
  QAction*  _reduceDelayAction;
};
} /* ----- end of Ui ----- */
#endif // MAINWINDOW_H
