#ifndef EulerEquations_Renderer_hpp
#define EulerEquations_Renderer_hpp

#include "Kernel.hpp"
#include "LoggingMacros.hpp"
#include "qcustomplot.hpp"

#include <QtCore>

namespace EulerEquations {
class Renderer:  public QThread {
  Q_OBJECT

public:
  Renderer();

  Renderer(
    Renderer const& other);

  ~Renderer();

  Renderer const&
  operator=(
    Renderer const& other) {
    return *this;
  }

  double const&
  time() const { return _time; }

  double
  dX() const { return _dX; }

  int
  xSize() const { return _xSize; }

  int const&
  delay() const { return _delay; }

  bool const&
  isPaused() const { return _isPaused; }

  void
  stop() {
    if (_isPaused)
      toggleSimulation();

    pStop();
    //   QObject::staticMetaObject.invokeMethod(this, "pStop",
    //                                          Qt::QueuedConnection);
  }

public
  slots:
  void
  delay(int const& delay) {
    if (delay <= 0)
      _delay = 0;
    else
      _delay = delay;
  }

  void
  toggleSimulation() {
    _mutex.lock();

    if (_isPaused)
      _waitCondition.wakeAll();

    _isPaused = !_isPaused;
    _mutex.unlock();
  }

public:
signals:
  void
  densityUpdated(QCPDataMap* map);

  void
  velocityUpdated(QCPDataMap* map);

  void
  pressureUpdated(QCPDataMap* map);

protected:
  void
  createMaps() {
    _densityMap  = new QCPDataMap();
    _velocityMap = new QCPDataMap();
    _pressureMap = new QCPDataMap();
  }

  virtual void
  run() {
    PRINT("Simulation Started at Thread %d", QThread::currentThread());
    // _isFinished = false;
    double position = 0.0;

    double const* vector = _kernel.solution();

    createMaps();

    for (int i = 0; i < _xSize; ++i) {
      (*_densityMap)[position]  = QCPData(position, vector[0]);
      (*_velocityMap)[position] = QCPData(position, vector[1]);
      (*_pressureMap)[position] = QCPData(position, vector[2]);
      vector                   += 3;
      position                 += _dX;
    }

    emitUpdate();

    while (!isFinishedCondition()) {
      _mutex.lock();

      if (_isPaused)
        _waitCondition.wait(&_mutex);

      _mutex.unlock();

      if (isFinishedCondition())
        break;

      QThread::msleep(_delay);
      _kernel.run(_dT, _time);
      double const* vector = _kernel.solution();

      if (isFinishedCondition())
        break;

      position = 0.0;
      createMaps();

      for (int i = 0; i < _xSize; ++i) {
        (*_densityMap)[position]  = QCPData(position, vector[0]);
        (*_velocityMap)[position] = QCPData(position, vector[1]);
        (*_pressureMap)[position] = QCPData(position, vector[2]);
        vector                   += 3;
        position                 += _dX;
      }

      emitUpdate();

      ++_iterationNumber;
      _time += _dT;
    }

    _kernel.release();
    PRINT("Simulation Finished");
  }

  void
  emitUpdate() {
    emit densityUpdated(_densityMap);
    emit velocityUpdated(_velocityMap);
    emit pressureUpdated(_pressureMap);
  }

  bool
  isFinishedCondition() {
    QMutexLocker locker(&_finishMutex);
    return _isFinished;
  }

  void
  pStop() {
    QMutexLocker locker(&_finishMutex);
    _isFinished = true;
  }

private:
  double      _dT;
  double      _dX;
  int         _xSize;
  double      _safetyFactor;
  int         _iterationNumber;
  double      _time;
  Kernel      _kernel;
  QCPDataMap* _densityMap;
  QCPDataMap* _velocityMap;
  QCPDataMap* _pressureMap;

  bool           _isPaused;
  bool           _isFinished;
  int            _delay;
  QMutex         _mutex;
  QWaitCondition _waitCondition;
  QMutex         _finishMutex;
  QMutex         _finishMutex2;
  QWaitCondition _finishCondition;
}; /* ----- end of Renderer ----- */
} /* ----- end of EulerEquations ----- */
#endif /* ----- end EulerEquations_Renderer_hpp ----- */
