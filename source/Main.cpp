#include <QApplication.h>
#include <QString.h>

#include "Kernel.hpp"
#include "MainWindow.h"

int
main(int argc, char* argv[]) {
  using namespace EulerEquations;

  Q_INIT_RESOURCE(resource);

  QApplication::setGraphicsSystem("raster");
  QApplication   a(argc, argv);
  Ui::MainWindow w;
  w.showMaximized();
  return a.exec();
}
