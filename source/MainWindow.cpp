#include "mainwindow.h"

using namespace Ui;

MainWindow::
MainWindow(QWidget* parent):
  QMainWindow(parent) {
  // Initialization
  _mdiArea      = new QMdiArea;
  _densityPlot  = new Plot;
  _velocityPlot = new Plot;
  _pressurePlot = new Plot;

  createActions();
  createToolBars();

  // MdiArea configuring
  _mdiArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
  _mdiArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
  setCentralWidget(_mdiArea);

  // the Density plot
  _densityPlot->xAxis->setRange(0, _renderer.xSize() * _renderer.dX());
  _densityPlot->yAxis->setRange(-0.5, 1.5);
  _densityPlot->setTitle("Density");
  _densityPlot->xAxis->setLabel("X");
  _densityPlot->yAxis->setLabel("Density");

  // Create the density graph
  _densityPlot->addGraph();
  _densityPlot->graph()->setName("Density");
  _densityPlot->graph()->setLineStyle((QCPGraph::LineStyle)1);
  QPen graphPen;
  graphPen.setColor(QColor(204, 51, 255));
  graphPen.setWidthF(1.0);
  _densityPlot->graph()->setPen(graphPen);
  _densityPlot->graph()->setScatterStyle(QCP::ScatterStyle::ssDiamond);
  _densityPlot->graph()->setScatterSize(4);

  // the Velocity Plot
  _velocityPlot->xAxis->setRange(0, _renderer.xSize() * _renderer.dX());
  _velocityPlot->yAxis->setRange(-0.5, 1.5);
  _velocityPlot->setTitle("Velocity");
  _velocityPlot->xAxis->setLabel("X");
  _velocityPlot->yAxis->setLabel("Velocity");

  // Create the Velocity graph
  _velocityPlot->addGraph();
  _velocityPlot->graph()->setName("Velocity");
  _velocityPlot->graph()->setLineStyle((QCPGraph::LineStyle)1);
  graphPen.setColor(QColor(255, 51, 0));
  graphPen.setWidthF(1.0);
  _velocityPlot->graph()->setPen(graphPen);
  _velocityPlot->graph()->setScatterStyle(QCP::ScatterStyle::ssCross);
  _velocityPlot->graph()->setScatterSize(4);

  // the Pressue plot
  _pressurePlot->xAxis->setRange(0, _renderer.xSize() * _renderer.dX());
  _pressurePlot->yAxis->setRange(-0.5, 1.5);
  _pressurePlot->setTitle("Pressure");
  _pressurePlot->xAxis->setLabel("X");
  _pressurePlot->yAxis->setLabel("Pressure");

  // Create the Pressure graph
  _pressurePlot->addGraph();
  _pressurePlot->graph()->setName("Pressure");
  _pressurePlot->graph()->setLineStyle((QCPGraph::LineStyle)1);
  graphPen.setColor(QColor(0, 51, 205));
  graphPen.setWidthF(1.0);
  _pressurePlot->graph()->setPen(graphPen);
  _pressurePlot->graph()->setScatterStyle(QCP::ScatterStyle::ssCircle);
  _pressurePlot->graph()->setScatterSize(4);

  // Adding plots
  _mdiArea->addSubWindow(_densityPlot);
  _mdiArea->addSubWindow(_velocityPlot);
  _mdiArea->addSubWindow(_pressurePlot);
  //   setWindowFlags( Qt::WindowMinMaxButtonsHint);
  // _mdiArea->addSubWindow(_velocityPlot)->
  //   setWindowFlags( Qt::WindowMinMaxButtonsHint);
  // _mdiArea->addSubWindow(_pressurePlot)->
  //   setWindowFlags( Qt::WindowMinMaxButtonsHint);

  _mdiArea->tileSubWindows();

  QObject::connect(&_renderer,
                   SIGNAL(finished()),
                   this,
                   SLOT(close()), Qt::QueuedConnection);
  QObject::connect(&_renderer,
                   SIGNAL(densityUpdated(QCPDataMap*)),
                   _densityPlot,
                   SLOT(render(QCPDataMap*)), Qt::BlockingQueuedConnection);
  QObject::connect(&_renderer,
                   SIGNAL(velocityUpdated(QCPDataMap*)),
                   _velocityPlot,
                   SLOT(render(QCPDataMap*)), Qt::BlockingQueuedConnection);
  QObject::connect(&_renderer,
                   SIGNAL(pressureUpdated(QCPDataMap*)),
                   _pressurePlot,
                   SLOT(render(QCPDataMap*)), Qt::BlockingQueuedConnection);
}

MainWindow::
~MainWindow() {}

void
MainWindow::
showEvent(QShowEvent* event) {
  QMainWindow::showEvent(event);
  static bool firstStart = true;

  if (firstStart)
    _renderer.start();

  firstStart = false;
}

void
MainWindow::
closeEvent(QCloseEvent* event) {
  //qDebug() << "CLode event";
  if (_renderer.isRunning()) {
    _renderer.stop();
    event->ignore();
  }
}

void
MainWindow::
createActions() {
  _runSimulationAction = new QAction(QIcon(":/image/run.png"), tr(
                                       "Run Simulation"), this);
  connect(_runSimulationAction, SIGNAL(triggered()), this, SLOT(
            runSimulation()));

  _layoutAction = new QAction(QIcon(":/image/layout.png"), tr(
                                "Tile Windows"), this);
  connect(_layoutAction, SIGNAL(triggered()), this, SLOT(tileMdiWindows()));

  _autoRescaleAction =
    new QAction(QIcon(":/image/autoRescale.png"), tr(
                  "Toggle Auto-Rescale for Axises "), this);
  connect(_autoRescaleAction, SIGNAL(triggered()), this,
          SLOT(toggleAutoReslcalePlots()));

  _rescaleAction = new QAction(QIcon(":/image/rescale.png"), tr(
                                 "Rescale Axis"), this);
  connect(_rescaleAction, SIGNAL(triggered()), this, SLOT(reslcalePlots()));

  _increaseDelayAction =
    new QAction(QIcon(":/image/increaseDelay.png"),
                tr("Increase Delay"), this);
  connect(_increaseDelayAction,
          SIGNAL(triggered()), this,
          SLOT(increaseDelay()));

  _reduceDelayAction =
    new QAction(QIcon(":/image/reduceDelay.png"),
                tr("Reduce Delay"), this);
  connect(_reduceDelayAction,
          SIGNAL(triggered()), this,
          SLOT(reduceDelay()));
}

void
MainWindow::
createToolBars() {
  _toolBar = addToolBar(tr("Main Tool Bar"));
  _toolBar->addAction(_runSimulationAction);
  _toolBar->addAction(_reduceDelayAction);
  _toolBar->addAction(_layoutAction);
  _toolBar->addAction(_autoRescaleAction);
  _toolBar->addAction(_rescaleAction);
  _toolBar->addAction(_increaseDelayAction);
  _toolBar->addAction(_reduceDelayAction);
}

void
MainWindow::
runSimulation() {
  _renderer.toggleSimulation();
}

void
MainWindow::
tileMdiWindows() {
  _mdiArea->tileSubWindows();
}

void
MainWindow::
toggleAutoReslcalePlots() {
  _densityPlot->toggleAutoRescale();
  _velocityPlot->toggleAutoRescale();
  _pressurePlot->toggleAutoRescale();
}

void
MainWindow::
reslcalePlots() {
  _densityPlot->rescaleAxes();
  _densityPlot->replot();
  _velocityPlot->rescaleAxes();
  _velocityPlot->replot();
  _pressurePlot->rescaleAxes();
  _pressurePlot->replot();
}

void
MainWindow::
increaseDelay() {
  _renderer.delay(_renderer.delay() + 50);
}

void
MainWindow::
reduceDelay() {
  _renderer.delay(_renderer.delay() - 50);
}

void
MainWindow::
close() {
  QMainWindow::close();
}
