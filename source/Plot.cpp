#include "Plot.hpp"

using namespace Ui;

Plot::
Plot(QWidget* parent):
  QCustomPlot(parent),
  _isAutoRescale(false) {
  setInteractions(QCustomPlot::iRangeDrag | QCustomPlot::iRangeZoom | QCustomPlot::iSelectAxes |
                  QCustomPlot::iSelectLegend | QCustomPlot::iSelectPlottables |
                  QCustomPlot::iSelectTitle);
  setRangeDrag(Qt::Horizontal | Qt::Vertical);
  setRangeZoom(Qt::Horizontal | Qt::Vertical);
  setupFullAxesBox();
  xAxis2->setVisible(true);
  xAxis2->setTickLabels(true);
  yAxis2->setVisible(true);
  yAxis2->setTickLabels(true);
  legend->setVisible(true);
  QFont legendFont = font();
  legendFont.setPointSize(12);
  legend->setFont(legendFont);
  legend->setSelectedFont(legendFont);
  legend->setSelectable(QCPLegend::spItems);
  // connect slot that ties some axis selections together (especially
  // opposite axes):
  connect(this, SIGNAL(selectionChangedByUser()), this,
          SLOT(selectionChangedSlot()));
  // connect slots that takes care that when an axis is selected, only that
  // direction can be dragged and zoomed:
  connect(this, SIGNAL(mousePress(QMouseEvent*)), this,
          SLOT(mousePressSlot()));
  connect(this, SIGNAL(mouseWheel(QWheelEvent*)), this,
          SLOT(mouseWheelSlot()));
  // make bottom and left axes transfer their ranges to top and right axes:
  connect(this->xAxis, SIGNAL(rangeChanged(
                                QCPRange)), this->xAxis2,
          SLOT(setRange(QCPRange)));
  connect(this->yAxis, SIGNAL(rangeChanged(
                                QCPRange)), this->yAxis2,
          SLOT(setRange(QCPRange)));
}

void
Plot::
render(QCPDataMap* map) {
  graph()->setData(map, false);

  if (_isAutoRescale)
    rescaleAxes();

  replot();
}

void
Plot::
selectionChangedSlot() {
  /*
   *  normally, axis base line, axis tick labels and axis labels are selectable
   * separately, but we want
   *  the user only to be able to select the axis as a whole, so we tie the
   * selected states of the tick labels
   *  and the axis base line together. However, the axis label shall be
   * selectable individually.
   *
   *  The selection state of the left and right axes shall be synchronized as
   * well as the state of the
   *  bottom and top axes.
   *
   *  Further, we want to synchronize the selection of the graphs with the
   * selection state of the respective
   *  legend item belonging to that graph. So the user can select a graph by
   * either clicking on the graph itself
   *  or on its legend item.
   */

  // make top and bottom axes be selected synchronously, and handle axis and
  // tick labels as one selectable object:
  if (xAxis->selected().testFlag(QCPAxis::spAxis) ||
      xAxis->selected().testFlag(QCPAxis::spTickLabels) ||
      xAxis2->selected().testFlag(QCPAxis::spAxis) ||
      xAxis2->selected().testFlag(QCPAxis::spTickLabels)) {
    xAxis2->setSelected(QCPAxis::spAxis |
                        QCPAxis::spTickLabels);
    xAxis->setSelected(QCPAxis::spAxis |
                       QCPAxis::spTickLabels);
  }

  // make left and right axes be selected synchronously, and handle axis and
  // tick labels as one selectable object:
  if (yAxis->selected().testFlag(QCPAxis::spAxis) ||
      yAxis->selected().testFlag(QCPAxis::spTickLabels) ||
      yAxis2->selected().testFlag(QCPAxis::spAxis) ||
      yAxis2->selected().testFlag(QCPAxis::spTickLabels)) {
    yAxis2->setSelected(QCPAxis::spAxis |
                        QCPAxis::spTickLabels);
    yAxis->setSelected(QCPAxis::spAxis |
                       QCPAxis::spTickLabels);
  }

  // synchronize selection of graphs with selection of corresponding legend
  // items:
  for (int i = 0; i < graphCount(); ++i) {
    QCPGraph*               graph_ = graph(i);
    QCPPlottableLegendItem* item   = legend->itemWithPlottable(
      graph_);

    if (item->selected() || graph_->selected()) {
      item->setSelected(true);
      graph_->setSelected(true);
    }
  }
}

void
Plot::
mousePressSlot() {
  // if an axis is selected, only allow the direction of that axis to be
  // dragged
  // if no axis is selected, both directions may be dragged

  if (xAxis->selected().testFlag(QCPAxis::spAxis))
    setRangeDrag(xAxis->orientation());
  else if (yAxis->selected().testFlag(QCPAxis::spAxis))
    setRangeDrag(yAxis->orientation());
  else
    setRangeDrag(Qt::Horizontal | Qt::Vertical);
}

void
Plot::
mouseWheelSlot() {
  // if an axis is selected, only allow the direction of that axis to be zoomed
  // if no axis is selected, both directions may be zoomed

  if (xAxis->selected().testFlag(QCPAxis::spAxis))
    setRangeZoom(xAxis->orientation());
  else if (yAxis->selected().testFlag(QCPAxis::spAxis))
    setRangeZoom(yAxis->orientation());
  else
    setRangeZoom(Qt::Horizontal | Qt::Vertical);
}
