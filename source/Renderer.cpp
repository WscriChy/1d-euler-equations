#include "Renderer.hpp"

using namespace EulerEquations;

Renderer::
Renderer():
  _dT(0.0),
  _dX(0.01),
  _xSize(100),
  _safetyFactor(0.5),
  _iterationNumber(0),
  _time(0),
  _densityMap(new QCPDataMap()),
  _velocityMap(new QCPDataMap()),
  _pressureMap(new QCPDataMap()),
  _delay(100),
  _isPaused(true),
  _isFinished(false),
  _finishMutex(QMutex::Recursive) {
  _kernel.sourceFileName(
    QString("%1/Kernel.cl").arg(QCoreApplication::applicationDirPath ()).toStdString());
  _kernel.dX(_dX);
  _kernel.xSize(_xSize);
  _kernel.safetyFactor(_safetyFactor);
  _kernel.initialize();
}

Renderer::
Renderer(
  Renderer const& other) {}

Renderer::
~Renderer() {}
