#ifndef LoggingMacros_hpp
#define LoggingMacros_hpp

#include <cstdio>
#include <string>
#include <cstring>

namespace Detail {
template <typename T>
inline char const*
convert(T const& t) {
  return t;
} /* ----- end of convert ----- */

template <>
inline char const* convert<
  std::string
  >(std::string const& t) {
  return t.c_str();
} /* ----- end of convert ----- */
} /* ----- end of Detail ----- */

#define PRINT(x, ...)                                       \
  if (true) {                                               \
    char const* message = Detail::convert(x);               \
    char        buffer[strlen(message)];                    \
    std::sprintf(buffer, message, ## __VA_ARGS__);          \
    std::printf("%s:%d |  %s\n", __FILE__, __LINE__, buffer); \
  }

#endif /* ----- end LoggingMacros_hpp ----- */
