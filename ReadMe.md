1D Euler Equations
=

## Description

The solver is based on the Godunov's scheme with the Roe linearization solver for the Riemann problem.

## Requirements

+ `OpenCl` is used for the solver implementation;

+ `Qt` - for Gui.

    Components:

    + `QtCore`

    + `QtGui`

## Building/Installation

### Building

    cmake/configure.py
    cmake/build.py

By default, the project is built in the release mode. The switch between
release and debug modes could be done by adding `-variant Release` and `-variant Debug`, respectively.

### Installation

    cmake/install.py

Again, by default, the installation is executed for the release build. To
install the debug build the flag `variant` could be used in the same fashion as for the building:

    -variant Release - to install the release build;
    -variant Debug   - to install the debug build.
